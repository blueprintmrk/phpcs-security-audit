FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM composer:2.0

# Create php user and group
RUN addgroup -g 1000 php
RUN adduser -u 1000 -D -h /home/php -G php php

# The php user below doesn't have permission to create a file in /etc/ssl/certs
# or /etc/gitconfig, this RUN command creates files that the analyzer can write to.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:php /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    touch /etc/gitconfig && \
    chown root:php /etc/gitconfig && \
    chmod g+w /etc/gitconfig

USER php:php
WORKDIR /home/php/

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-2.0.1}

# Install phpcs-security-audit and add standards to PHP CodeSniffer
RUN composer require --dev pheromone/phpcs-security-audit $SCANNER_VERSION
RUN ["mv", "vendor/pheromone/phpcs-security-audit/Security", "vendor/squizlabs/php_codesniffer/src/Standards/"]
RUN ["mv", "vendor/pheromone/phpcs-security-audit/example_base_ruleset.xml", "."]
RUN ["mv", "vendor/pheromone/phpcs-security-audit/example_drupal7_ruleset.xml", "."]
RUN ["mv", "vendor/pheromone/phpcs-security-audit/tests.php", "."]
COPY --chown=php:php ruleset.xml .

COPY --from=build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
